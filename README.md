# INTRODUCTION #
This is a password generator for Metroid, a video game for the Nintendo Entertainment System. Metroid was released in North America back in August of 1987.

The Japanese version of the game, which was released for the Famicom Disk System a year earlier in August of 1986, utilized an internal battery in the game's cartridge to keep track of a player's progress while playing the game. Unfortunately, the North American release did not use a battery and instead used a sophisticated password
system to record what state the game was in; this was most likely done to reduce production costs as internal batteries in game cartridges were expensive to manufacture.

# HOW IS A METROID PASSWORD GENERATED? #
When the player dies in-game, a 144-bit string is created to keep track of the player's progress up until when Samus (the main character) died. The password records the bosses the player has defeated along with what power-ups have been collected (a '1' bit means a boss has been defeated/item has been taken and a '0' bit means otherwise). The number of missiles Samus has along with the game time, which is measured in game ticks (1 game tick is approximately equal to 4.27 seconds on a North American NES or 5.12 seconds on a European NES), are also recorded.

After creating the 144-bit string, the game runs the following algorithm to generate a Metroid password.

1. Calculating the checksum - The first 136 bits are added together, and the sum is to be bit-wise AND with 255. The result is the checksum.

2. Bit-shifting the first 128 bits - Bit-shifting is used to encrypt the password. The shift byte, which is stored in the second to last byte of the 144-bit string, tells the game how many times the first 128 bits need to be right-shifted by. After shifting the 128 bits X times, where X is the value of the shift byte, the game moves on to the next step. The checksum is then stored in the last 8 bits of the 144-bit string. Note that having a shift byte value of 0 is allowed; this just means that the password will not be encrypted and the game will not care.

3. Divide the 144-bit string into chunks and create a Metroid Password - Now that the first 128 bits have been properly rotated, the 144-bit string is to be divided into 24 chunks; each chunk must be 6 bits in size. Each chunk is then "converted" to a letter by converting the 6-bit chunk into a decimal number, then seeing what Metroid letter maps to that number.

For example, "001001" would map to the Metroid letter '9' and "110110" would map to the Metroid letter 's'.

The individual Metroid letters are then concatenated to form the final password.

# ACKNOWLEDGEMENTS #
Special thanks to 

* John David Ratliff for creating a Metroid Password Guide on Gamefaqs; I referred to it many times for help/clarification when writing this program. His guide describes how the password system works in greater detail.

* Void_attack for making the executable icon background transparent