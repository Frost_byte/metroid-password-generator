import javax.swing.JOptionPane;
import org.apache.commons.lang3.*;

public class Algorithm {
	
	// Returns a corresponding Metroid Alphabet Letter based on the 6 bit number provided
	static char Get_Metroid_Alphabet_Letter (int offset)
	{
		switch (offset)
		{
		case 0: return '0';
		case 1: return '1';
		case 2: return '2';
		case 3: return '3';
		case 4: return '4';
		case 5: return '5';
		case 6: return '6';
		case 7: return '7';
		case 8: return '8';
		case 9: return '9';
		case 10: return 'A';
		case 11: return 'B';
		case 12: return 'C';
		case 13: return 'D';
		case 14: return 'E';
		case 15: return 'F';
		case 16: return 'G';
		case 17: return 'H';
		case 18: return 'I';
		case 19: return 'J';
		case 20: return 'K';
		case 21: return 'L';
		case 22: return 'M';
		case 23: return 'N';
		case 24: return 'O';
		case 25: return 'P';
		case 26: return 'Q';
		case 27: return 'R';
		case 28: return 'S';
		case 29: return 'T';
		case 30: return 'U';
		case 31: return 'V';
		case 32: return 'W';
		case 33: return 'X';
		case 34: return 'Y';
		case 35: return 'Z';
		case 36: return 'a';
		case 37: return 'b';
		case 38: return 'c';
		case 39: return 'd';
		case 40: return 'e';
		case 41: return 'f';
		case 42: return 'g';
		case 43: return 'h';
		case 44: return 'i';
		case 45: return 'j';
		case 46: return 'k';
		case 47: return 'l';
		case 48: return 'm';
		case 49: return 'n';
		case 50: return 'o';
		case 51: return 'p';
		case 52: return 'q';
		case 53: return 'r';
		case 54: return 's';
		case 55: return 't';
		case 56: return 'u';
		case 57: return 'v';
		case 58: return 'w';
		case 59: return 'x';
		case 60: return 'y';
		case 61: return 'z';
		case 62: return '?';
		case 63: return '-';
		case 64: return ' '; // Should never return this because max value of a 6 bit number is 63
		default: return '\0';
		}
	}
	
	// Computes a checksum 
	static String Calculate_Checksum (String bit_pattern)
	{
		int total = 0;
		int value;
		
		for (int counter = 0; counter < bit_pattern.length(); counter = counter + 8)
		{
			
			value = Integer.parseInt(bit_pattern.substring(counter, counter + 8), 2);
			total = total + value;
		}
		
		
		total = total & 255;
	
		String checksum = Integer.toBinaryString(total);
		checksum = StringUtils.leftPad(checksum, 8, "0");
		return checksum;
	}
	
	// Shifts the bit pattern a certain number of times (depends on the shift byte)
	static String Bit_Shift (String bit_string, String shift_byte)
	{
		int times_to_shift = Integer.parseInt(shift_byte, 2);
		String bit_pattern = bit_string.substring(0, bit_string.length() - 8);
		
		for (int counter = 0; counter < times_to_shift; counter++)
		{
			bit_pattern = bit_pattern.charAt(bit_pattern.length() - 1) + bit_pattern.substring(0, bit_pattern.length() - 1);
		}
		
		bit_pattern = bit_pattern + shift_byte;
		return bit_pattern;
	}
	
	// Prints password in a pop-up window
	static void Print_Password (String bit_pattern)
	{
		String password = "";
		String substring;
		char letter;
		
		for (int counter = 0; counter < bit_pattern.length(); counter = counter + 6)
		{
			substring = bit_pattern.substring(counter, counter + 6);
			letter = Get_Metroid_Alphabet_Letter(Integer.parseInt(substring, 2));
			password = password + letter;
			
			if (password.length() == 6)
			{
				password = password + " ";
			}
			
			else if (password.length() == 13)
			{
				password = password + " ";
			}
			
			else if (password.length() == 20) 
			{
				password = password + " ";
			}
			
		}
		
		JOptionPane.showMessageDialog(null, "Password is " + password);
		
		return;
	}
	

	

}
