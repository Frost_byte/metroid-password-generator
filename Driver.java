import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import org.apache.commons.lang3.StringUtils;
import java.awt.Color;

public class Driver extends JFrame implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	
	// Individual byte patterns
	private String byte_0 = "00000000";
	private String byte_1 = "00000000";
	private String byte_2 = "00000000";
	private String byte_3 = "00000000";
	private String byte_4 = "00000000";
	private String byte_5 = "00000000";
	private String byte_6 = "00000000";
	private String byte_7 = "00000000";
	private String byte_8 = "00000000";
	private String byte_9 = "00000000";
	private String missile_count_string = "00000000";
	private String game_time_string = "00000000000000000000000000000000"; 
	private String byte_15 = "00000000";
	private String checksum_string = "00000000";
	private String shift_byte_string = "00000000";
	
	// Checkboxes are stored in an array of type JCheckBox
	private JCheckBox checkbox [ ] = new JCheckBox[88];
	

	// Text fields with corresponding labels
	private JTextField missile_count_TF = new JTextField("0",20);
	private JTextField game_time_TF = new JTextField("0",20);
	private JTextField shift_byte_TF = new JTextField("0",20); 
	
	
	// Buttons
	private JButton set_missile_count = new JButton("Set number of missiles");
	private JButton set_game_time = new JButton("Set game time");
	private JButton set_shift_byte = new JButton("Set shift byte");
	private JButton generate_password = new JButton("Generate password");
	private JButton about = new JButton("About");
	
	// Panels
	private JPanel checkboxPanel = new JPanel();
	private JPanel textfield_panel = new JPanel();
	private JSplitPane split = new JSplitPane();
	
/*******************************************************************************************/	
	// Constructor
	public Driver(){
		super("Metroid Password Generator");
	
		ImageIcon img = new ImageIcon("C:/Users/Thomas/workspace/Metroid Password Generator/src/res/transparent.png");
		this.setIconImage(img.getImage()); 
		
		Font titleFont = new Font("Courier New", Font.PLAIN, 16); 
		
	    checkboxPanel.setLayout(new GridLayout(33,3)); 
	    checkboxPanel.setBorder(BorderFactory.createTitledBorder(checkboxPanel.getBorder(), "Password Checkboxes", TitledBorder.CENTER, TitledBorder.CENTER, titleFont)); 
	    checkboxPanel.setOpaque(true);
		checkboxPanel.setAlignmentY(JComponent.LEFT_ALIGNMENT); 
		checkboxPanel.setBackground(Color.WHITE);
		ActionListener actionListener = new ActionHandler();
		
		// Byte 0
		checkbox[0] = new JCheckBox("Bit 0 - Took Maru Mari");
		checkbox[1] = new JCheckBox("Bit 1 - Missile Container (Brinstar)");
		checkbox[2] = new JCheckBox("Bit 2 - Red Door (Long Beam)");
		checkbox[3] = new JCheckBox("Bit 3 - Red Door (Tourian Bridge)");
		checkbox[4] = new JCheckBox("Bit 4 - Energy Tank (Brinstar)");
		checkbox[5] = new JCheckBox("Bit 5 - Red Door (Bombs)");
		checkbox[6] = new JCheckBox("Bit 6 - Took Bombs");
		checkbox[7] = new JCheckBox("Bit 7 - Red Door (Ice Beam Brinstar)");
		
		// Byte 1
		checkbox[8] = new JCheckBox("Bit 8 - Missile Container (Brinstar)");
		checkbox[9] = new JCheckBox("Bit 9 - Energy Tank (Brinstar)");
		checkbox[10] = new JCheckBox("Bit 10 - Red Door (Varia Suit)");
		checkbox[11] = new JCheckBox("Bit 11 - Took Varia Suit");
		checkbox[12] = new JCheckBox("Bit 12 - Energy Tank (Brinstar)");
		checkbox[13] = new JCheckBox("Bit 13 - Missile Container (Norfair)");
		checkbox[14] = new JCheckBox("Bit 14 - Missile Container (Norfair)");
		checkbox[15] = new JCheckBox("Bit 15 - Red Door (Ice Beam Norfair)"); 
		
		// Byte 2
		checkbox[16] = new JCheckBox("Bit 16 - Missile Container (Norfair)"); 
		checkbox[17] = new JCheckBox("Bit 17 - Missile Container (Norfair)"); 
		checkbox[18] = new JCheckBox("Bit 18 - Missile Container (Norfair)"); 
		checkbox[19] = new JCheckBox("Bit 19 - Missile Container (Norfair)"); 
		checkbox[20] = new JCheckBox("Bit 20 - Missile Container (Norfair)"); 
		checkbox[21] = new JCheckBox("Bit 21 - Missile Container (Norfair)"); 
		checkbox[22] = new JCheckBox("Bit 22 - Missile Container (Norfair)"); 
		checkbox[23] = new JCheckBox("Bit 23 - Red Door (High Jump Boots)"); 
		
		// Byte 3
		checkbox[24] = new JCheckBox("Bit 24 - Took High Jump Boots"); 
		checkbox[25] = new JCheckBox("Bit 25 - Red Door (Screw Attack)"); 
		checkbox[26] = new JCheckBox("Bit 26 - Took Screw Attack"); 
		checkbox[27] = new JCheckBox("Bit 27 - Missile Container (Norfair)"); 
		checkbox[28] = new JCheckBox("Bit 28 - Missile Container (Norfair)"); 
		checkbox[29] = new JCheckBox("Bit 29 - Red Door (Wave Beam)"); 
		checkbox[30] = new JCheckBox("Bit 30 - Energy Tank (Norfair)"); 
		checkbox[31] = new JCheckBox("Bit 31 - Missile Container (Norfair)"); 
		
		// Byte 4
		checkbox[32] = new JCheckBox("Bit 32 - Red Door (Kraid's Lair)"); 
		checkbox[33] = new JCheckBox("Bit 33 - Missile Container (Kraid's Lair)"); 
		checkbox[34] = new JCheckBox("Bit 34 - Missile Container (Kraid's Lair)"); 
		checkbox[35] = new JCheckBox("Bit 35 - Red Door (Kraid's Lair)"); 
		checkbox[36] = new JCheckBox("Bit 36 - Energy Tank (Kraid's Lair)"); 
		checkbox[37] = new JCheckBox("Bit 37 - Red Door (Kraid's Lair)"); 
		checkbox[38] = new JCheckBox("Bit 38 - Red Door (Kraid's Lair)"); 
		checkbox[39] = new JCheckBox("Bit 39 - Missile Container (Kraid's Lair)"); 
		
		// Byte 5
		checkbox[40] = new JCheckBox("Bit 40 - Missile Container (Kraid's Lair)"); 
		checkbox[41] = new JCheckBox("Bit 41 - Red Door (Kraid's Room)"); 
		checkbox[42] = new JCheckBox("Bit 42 - Energy Tank (Kraid's Room)"); 
		checkbox[43] = new JCheckBox("Bit 43 - Missile Container (Ridley's Lair)"); 
		checkbox[44] = new JCheckBox("Bit 44 - Red Door (Ridley's Lair)"); 
		checkbox[45] = new JCheckBox("Bit 45 - Energy Tank (Ridley's Lair)"); 
		checkbox[46] = new JCheckBox("Bit 46 - Missile Container (Ridley's Lair)"); 
		checkbox[47] = new JCheckBox("Bit 47 - Yellow Door (Ridley's Room)"); 
		
		// Byte 6
		checkbox[48] = new JCheckBox("Bit 48 - Energy Tank (Room behind Ridley)"); 
		checkbox[49] = new JCheckBox("Bit 49 - Missile Container (Ridley's Lair)"); 
		checkbox[50] = new JCheckBox("Bit 50 - Yellow Door (Tourian)"); 
		checkbox[51] = new JCheckBox("Bit 51 - Red Door (Tourian)"); 
		checkbox[52] = new JCheckBox("Bit 52 - Red Door (Tourian)"); 
		checkbox[53] = new JCheckBox("Bit 53 - Killed Zebetite 1"); 
		checkbox[54] = new JCheckBox("Bit 54 - Killed Zebetite 2"); 
		checkbox[55] = new JCheckBox("Bit 55 - Killed Zebetite 3"); 
		
		// Byte 7
		checkbox[56] = new JCheckBox("Bit 56 - Killed Zebetite 4"); 
		checkbox[57] = new JCheckBox("Bit 57 - Killed Zebetite 5"); 
		checkbox[58] = new JCheckBox("Bit 58 - Killed Mother Brain"); 
		checkbox[59] = new JCheckBox("Bit 59 - ???"); 
		checkbox[60] = new JCheckBox("Bit 60 - ???"); 
		checkbox[61] = new JCheckBox("Bit 61 - ???"); 
		checkbox[62] = new JCheckBox("Bit 62 - ???"); 
		checkbox[63] = new JCheckBox("Bit 63 - ???"); 
		
		// Byte 8
		checkbox[64] = new JCheckBox("Bit 64 - Start in Norfair"); 
		checkbox[65] = new JCheckBox("Bit 65 - Start in Kraid's Lair"); 
		checkbox[66] = new JCheckBox("Bit 66 - Start in Ridley's Lair"); 
		checkbox[67] = new JCheckBox("Bit 67 - Reset"); 
		checkbox[68] = new JCheckBox("Bit 68 - ???"); 
		checkbox[69] = new JCheckBox("Bit 69 - ???"); 
		checkbox[70] = new JCheckBox("Bit 70 - ???"); 
		checkbox[71] = new JCheckBox("Bit 71 - Equip Leotard"); 
		
		// Byte 9
		checkbox[72] = new JCheckBox("Bit 72 - Samus has Bombs"); 
		checkbox[73] = new JCheckBox("Bit 73 - Samus has High Jump Boots"); 
		checkbox[74] = new JCheckBox("Bit 74 - Samus has Long Beam"); 
		checkbox[75] = new JCheckBox("Bit 75 - Samus has Screw Attack"); 
		checkbox[76] = new JCheckBox("Bit 76 - Samus has Maru Mari"); 
		checkbox[77] = new JCheckBox("Bit 77 - Samus has Varia Suit"); 
		checkbox[78] = new JCheckBox("Bit 78 - Samus has Wave Beam"); 
		checkbox[79] = new JCheckBox("Bit 79 - Samus has Ice Beam"); 
		
		// Byte 15
		checkbox[80] = new JCheckBox("Bit 80 - ???"); 
		checkbox[81] = new JCheckBox("Bit 81 - ???"); 
		checkbox[82] = new JCheckBox("Bit 82 - ???"); 
		checkbox[83] = new JCheckBox("Bit 83 - ???"); 
		checkbox[84] = new JCheckBox("Bit 84 - Killed Ridley"); 
		checkbox[85] = new JCheckBox("Bit 85 - Ridley Statue Raised"); 
		checkbox[86] = new JCheckBox("Bit 86 - Killed Kraid"); 
		checkbox[87] = new JCheckBox("Bit 87 - Kraid Statue Raised"); 
		
		Font myFont = new Font("Courier New", Font.PLAIN, 12);
		for (int counter = 0; counter < 88; counter ++)
		{
			checkbox[counter].setBackground(Color.WHITE);
			checkboxPanel.add(checkbox[counter]);
			checkbox[counter].addActionListener(actionListener);
			checkbox[counter].setFont(myFont);
			
		}
		
		
		// Add text fields to different panel 
		
		textfield_panel.setLayout(new GridLayout(4,1));
		textfield_panel.setAlignmentY(JComponent.LEFT_ALIGNMENT);
		textfield_panel.setBackground(Color.WHITE);
		textfield_panel.setBorder(BorderFactory.createTitledBorder(textfield_panel.getBorder(), "Miscellaneous", TitledBorder.CENTER, TitledBorder.CENTER, titleFont)); 
	 
		
		
		textfield_panel.add(missile_count_TF);
		missile_count_TF.addActionListener(actionListener);
		missile_count_TF.setFont(myFont);
		textfield_panel.add(set_missile_count);
		set_missile_count.addActionListener(this);
		set_missile_count.setFont(myFont); 
	
		
		textfield_panel.add(game_time_TF);
		game_time_TF.addActionListener(actionListener);
		game_time_TF.setFont(myFont);
		textfield_panel.add(set_game_time);
		set_game_time.addActionListener(this);
		set_game_time.setFont(myFont);
		
		textfield_panel.add(shift_byte_TF);
		shift_byte_TF.addActionListener(actionListener);
		shift_byte_TF.setFont(myFont);
		textfield_panel.add(set_shift_byte);
		set_shift_byte.addActionListener(actionListener);
		set_shift_byte.setFont(myFont); 
		
		textfield_panel.add(about);
		about.addActionListener(actionListener);
		about.setFont(myFont);
		
		checkboxPanel.add(generate_password);
		generate_password.addActionListener(actionListener);
		generate_password.setFont(myFont);
		
		// Split the checkbox and textfield panels
		split.setDividerSize(0);
	    split.setDividerLocation(150);
	    split.setOrientation(JSplitPane.VERTICAL_SPLIT);
	    split.setTopComponent(textfield_panel);
	    split.setBottomComponent(checkboxPanel); 
	    
	    this.add(split);  
	    this.setVisible(true);
	    this.pack(); 
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
	}
	
	
/*******************************************************************************************/
	// ActionHandler for check boxes
	class ActionHandler implements ActionListener {
	@Override
	
	public void actionPerformed(ActionEvent event) { 
		// TODO Auto-generated method stub
		
		// Shift Byte
		if (event.getSource() == set_shift_byte)
		{
			String input = shift_byte_TF.getText();
			
			if (input.length()==0)
			{
				shift_byte_string = "00000000"; 
				return;
			}
			
			char [] array = input.toCharArray();
			
			for (int counter = 0; counter < input.length(); counter++)
			{
				if (Character.isDigit(array[counter]) == false)
				{
					JOptionPane.showMessageDialog(Driver.this, "Input can only have numbers");  
					return;
				} 
			}
			
			int temp = Integer.parseInt(input);
			
			if (temp < 0 || temp > 255)
			{
				JOptionPane.showMessageDialog(Driver.this, "Range is 0 <= input <= 255"); 
				return;
			}
			
			String temp2 = Integer.toBinaryString(temp);
			temp2 = StringUtils.leftPad(temp2, 8, "0");
			shift_byte_string = temp2; 
			return;
		} 
		
		else if (event.getSource() == about)
		{
			JOptionPane.showMessageDialog(null, "� THOMAS WONG (AKA Frost_byte), JUNE 2017, VERSION 1.0");
		}
		
		else if (event.getSource() == generate_password)
		{
			String byte_0_rev, byte_1_rev, byte_2_rev, byte_3_rev, byte_4_rev, byte_5_rev, byte_6_rev, byte_7_rev;
			String byte_8_rev, byte_9_rev, byte_15_rev; // missile_count_string_rev;
			
			byte_0_rev = new StringBuilder(byte_0).reverse().toString();
			byte_1_rev = new StringBuilder(byte_1).reverse().toString();
			byte_2_rev = new StringBuilder(byte_2).reverse().toString();
			byte_3_rev = new StringBuilder(byte_3).reverse().toString();
			byte_4_rev = new StringBuilder(byte_4).reverse().toString();
			byte_5_rev = new StringBuilder(byte_5).reverse().toString();
			byte_6_rev = new StringBuilder(byte_6).reverse().toString();
			byte_7_rev = new StringBuilder(byte_7).reverse().toString();
			byte_8_rev = new StringBuilder(byte_8).reverse().toString();
			byte_9_rev = new StringBuilder(byte_9).reverse().toString();
			byte_15_rev = new StringBuilder(byte_15).reverse().toString();
			// missile_count_string_rev = new StringBuilder(missile_count_string).reverse().toString();
			
			// Concatenate everything together
			String bit_pattern = byte_0_rev + byte_1_rev + byte_2_rev + byte_3_rev + byte_4_rev + byte_5_rev + byte_6_rev + byte_7_rev + byte_8_rev + byte_9_rev
					+ missile_count_string + game_time_string + byte_15_rev + shift_byte_string;
			
			// System.out.println("Game time string is " + game_time_string);
			
			// Run the algorithm
			String checksum_string = Algorithm.Calculate_Checksum(bit_pattern);
			bit_pattern = Algorithm.Bit_Shift(bit_pattern, shift_byte_string);
			bit_pattern = bit_pattern + checksum_string;
			Algorithm.Print_Password(bit_pattern);
			return;
		} 
		
		
		else if (event.getSource() != set_missile_count && event.getSource() != set_shift_byte && event.getSource() != set_game_time && event.getSource() != generate_password)
		{
		// ActionListeners for Checkboxes
		JCheckBox checkbox = (JCheckBox) event.getSource();
		
		if (checkbox.isSelected())
		{
			// Checkboxes corresponding to byte 0
			if (checkbox.getText().equals("Bit 0 - Took Maru Mari"))
			{
				char [ ] temporary = byte_0.toCharArray();
				temporary[0] = '1';
				byte_0 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 1 - Missile Container (Brinstar)"))
			{
				char [ ] temporary = byte_0.toCharArray();
				temporary[1] = '1';
				byte_0 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 2 - Red Door (Long Beam)"))
			{
				char [ ] temporary = byte_0.toCharArray();
				temporary[2] = '1';
				byte_0 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 3 - Red Door (Tourian Bridge)"))
			{
				char [ ] temporary = byte_0.toCharArray();
				temporary[3] = '1';
				byte_0 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 4 - Energy Tank (Brinstar)"))
			{
				char [ ] temporary = byte_0.toCharArray();
				temporary[4] = '1';
				byte_0 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 5 - Red Door (Bombs)"))
			{
				char [ ] temporary = byte_0.toCharArray();
				temporary[5] = '1';
				byte_0 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 6 - Took Bombs"))
			{
				char [ ] temporary = byte_0.toCharArray();
				temporary[6] = '1';
				byte_0 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 7 - Red Door (Ice Beam Brinstar)"))
			{
				char [ ] temporary = byte_0.toCharArray();
				temporary[7] = '1';
				byte_0 = String.valueOf(temporary);
			}
			
			// Checkboxes corresponding to byte 1 
			else if (checkbox.getText().equals("Bit 8 - Missile Container (Brinstar)"))
			{
				char [ ] temporary = byte_1.toCharArray();
				temporary[0] = '1';
				byte_1 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 9 - Energy Tank (Brinstar)"))
			{
				char [ ] temporary = byte_1.toCharArray();
				temporary[1] = '1';
				byte_1 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 10 - Red Door (Varia Suit)"))
			{
				char [ ] temporary = byte_1.toCharArray();
				temporary[2] = '1';
				byte_1 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 11 - Took Varia Suit"))
			{
				char [ ] temporary = byte_1.toCharArray();
				temporary[3] = '1';
				byte_1 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 12 - Energy Tank (Brinstar)"))
			{
				char [ ] temporary = byte_1.toCharArray();
				temporary[4] = '1';
				byte_1 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 13 - Missile Container (Norfair)"))
			{
				char [ ] temporary = byte_1.toCharArray();
				temporary[5] = '1';
				byte_1 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 14 - Missile Container (Norfair)"))
			{
				char [ ] temporary = byte_1.toCharArray();
				temporary[6] = '1';
				byte_1 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 15 - Red Door (Ice Beam Norfair)"))
			{
				char [ ] temporary = byte_1.toCharArray();
				temporary[7] = '1';
				byte_1 = String.valueOf(temporary);
			}
			
			// Checkboxes corresponding to bye 2
			else if (checkbox.getText().equals("Bit 16 - Missile Container (Norfair)"))
			{
				char [ ] temporary = byte_2.toCharArray();
				temporary[0] = '1';
				byte_2 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 17 - Missile Container (Norfair)"))
			{
				char [ ] temporary = byte_2.toCharArray();
				temporary[1] = '1';
				byte_2 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 18 - Missile Container (Norfair)"))
			{
				char [ ] temporary = byte_2.toCharArray();
				temporary[2] = '1';
				byte_2 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 19 - Missile Container (Norfair)"))
			{
				char [ ] temporary = byte_2.toCharArray();
				temporary[3] = '1';
				byte_2 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 20 - Missile Container (Norfair)"))
			{
				char [ ] temporary = byte_2.toCharArray();
				temporary[4] = '1';
				byte_2 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 21 - Missile Container (Norfair)"))
			{
				char [ ] temporary = byte_2.toCharArray();
				temporary[5] = '1';
				byte_2 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 22 - Missile Container (Norfair)"))
			{
				char [ ] temporary = byte_2.toCharArray();
				temporary[6] = '1';
				byte_2 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 23 - Red Door (High Jump Boots)"))
			{
				char [ ] temporary = byte_2.toCharArray();
				temporary[7] = '1';
				byte_2 = String.valueOf(temporary);
			}
			
			// Checkboxes corresponding to byte 3
			else if (checkbox.getText().equals("Bit 24 - Took High Jump Boots"))
			{
				char [ ] temporary = byte_3.toCharArray();
				temporary[0] = '1';
				byte_3 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 25 - Red Door (Screw Attack)"))
			{
				char [ ] temporary = byte_3.toCharArray();
				temporary[1] = '1';
				byte_3 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 26 - Took Screw Attack"))
			{
				char [ ] temporary = byte_3.toCharArray();
				temporary[2] = '1';
				byte_3 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 27 - Missile Container (Norfair)"))
			{
				char [ ] temporary = byte_3.toCharArray();
				temporary[3] = '1';
				byte_3 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 28 - Missile Container (Norfair)"))
			{
				char [ ] temporary = byte_3.toCharArray();
				temporary[4] = '1';
				byte_3 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 29 - Red Door (Wave Beam)"))
			{
				char [ ] temporary = byte_3.toCharArray();
				temporary[5] = '1';
				byte_3 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 30 - Energy Tank (Norfair)"))
			{
				char [ ] temporary = byte_3.toCharArray();
				temporary[6] = '1';
				byte_3 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 31 - Missile Container (Norfair)"))
			{
				char [ ] temporary = byte_3.toCharArray();
				temporary[7] = '1';
				byte_3 = String.valueOf(temporary);
			}
			
			// Checkboxes corresponding to byte 4
			else if (checkbox.getText().equals("Bit 32 - Red Door (Kraid's Lair)"))
			{
				char [ ] temporary = byte_4.toCharArray();
				temporary[0] = '1';
				byte_4 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 33 - Missile Container (Kraid's Lair)"))
			{
				char [ ] temporary = byte_4.toCharArray();
				temporary[1] = '1';
				byte_4 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 34 - Missile Container (Kraid's Lair)"))
			{
				char [ ] temporary = byte_4.toCharArray();
				temporary[2] = '1';
				byte_4 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 35 - Red Door (Kraid's Lair)"))
			{
				char [ ] temporary = byte_4.toCharArray();
				temporary[3] = '1';
				byte_4 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 36 - Energy Tank (Kraid's Lair)"))
			{
				char [ ] temporary = byte_4.toCharArray();
				temporary[4] = '1';
				byte_4 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 37 - Red Door (Kraid's Lair)"))
			{
				char [ ] temporary = byte_4.toCharArray();
				temporary[5] = '1';
				byte_4 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 38 - Red Door (Kraid's Lair)"))
			{
				char [ ] temporary = byte_4.toCharArray();
				temporary[6] = '1';
				byte_4 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 39 - Missile Container (Kraid's Lair)"))
			{
				char [ ] temporary = byte_4.toCharArray();
				temporary[7] = '1';
				byte_4 = String.valueOf(temporary);
			}
			
			// Checkboxes corresponding to byte 5
			else if (checkbox.getText().equals("Bit 40 - Missile Container (Kraid's Lair)"))
			{
				char [ ] temporary = byte_5.toCharArray();
				temporary[0] = '1';
				byte_5 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 41 - Red Door (Kraid's Room)"))
			{
				char [ ] temporary = byte_5.toCharArray();
				temporary[1] = '1';
				byte_5 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 42 - Energy Tank (Kraid's Room)"))
			{
				char [ ] temporary = byte_5.toCharArray();
				temporary[2] = '1';
				byte_5 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 43 - Missile Container (Ridley's Lair)"))
			{
				char [ ] temporary = byte_5.toCharArray();
				temporary[3] = '1';
				byte_5 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 44 - Red Door (Ridley's Lair)"))
			{
				char [ ] temporary = byte_5.toCharArray();
				temporary[4] = '1';
				byte_5 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 45 - Energy Tank (Ridley's Lair)"))
			{
				char [ ] temporary = byte_5.toCharArray();
				temporary[5] = '1';
				byte_5 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 46 - Missile Container (Ridley's Lair)"))
			{
				char [ ] temporary = byte_5.toCharArray();
				temporary[6] = '1';
				byte_5 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 47 - Yellow Door (Ridley's Room)"))
			{
				char [ ] temporary = byte_5.toCharArray();
				temporary[7] = '1';
				byte_5 = String.valueOf(temporary);
			}
			
			// Checkboxes corresponding to byte 6
			else if (checkbox.getText().equals("Bit 48 - Energy Tank (Room behind Ridley)"))
			{
				char [ ] temporary = byte_6.toCharArray();
				temporary[0] = '1';
				byte_6 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 49 - Missile Container (Ridley's Lair)"))
			{
				char [ ] temporary = byte_6.toCharArray();
				temporary[1] = '1';
				byte_6 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 50 - Yellow Door (Tourian)"))
			{
				char [ ] temporary = byte_6.toCharArray();
				temporary[2] = '1';
				byte_6 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 51 - Red Door (Tourian)"))
			{
				char [ ] temporary = byte_6.toCharArray();
				temporary[3] = '1';
				byte_6 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 52 - Red Door (Tourian)"))
			{
				char [ ] temporary = byte_6.toCharArray();
				temporary[4] = '1';
				byte_6 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 53 - Killed Zebetite 1"))
			{
				char [ ] temporary = byte_6.toCharArray();
				temporary[5] = '1';
				byte_6 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 54 - Killed Zebetite 2"))
			{
				char [ ] temporary = byte_6.toCharArray();
				temporary[6] = '1';
				byte_6 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 55 - Killed Zebetite 3"))
			{
				char [ ] temporary = byte_6.toCharArray();
				temporary[7] = '1';
				byte_6 = String.valueOf(temporary);
			}
			
			// Checkboxes corresponding to byte 7
			else if (checkbox.getText().equals("Bit 56 - Killed Zebetite 4"))
			{
				char [ ] temporary = byte_7.toCharArray();
				temporary[0] = '1';
				byte_7 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 57 - Killed Zebetite 5"))
			{
				char [ ] temporary = byte_7.toCharArray();
				temporary[1] = '1';
				byte_7 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 58 - Killed Mother Brain"))
			{
				char [ ] temporary = byte_7.toCharArray();
				temporary[2] = '1';
				byte_7 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 59 - ???"))
			{
				char [ ] temporary = byte_7.toCharArray();
				temporary[3] = '1';
				byte_7 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 60 - ???"))
			{
				char [ ] temporary = byte_7.toCharArray();
				temporary[4] = '1';
				byte_7 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 61 - ???"))
			{
				char [ ] temporary = byte_7.toCharArray();
				temporary[5] = '1';
				byte_7 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 62 - ???"))
			{
				char [ ] temporary = byte_7.toCharArray();
				temporary[6] = '1';
				byte_7 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 63 - ???"))
			{
				char [ ] temporary = byte_7.toCharArray();
				temporary[7] = '1';
				byte_7 = String.valueOf(temporary);
			}
			
			// Checkboxes corresponding to byte 8
			else if (checkbox.getText().equals("Bit 64 - Start in Norfair"))
			{
				char [ ] temporary = byte_8.toCharArray();
				temporary[0] = '1';
				byte_8 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 65 - Start in Kraid's Lair"))
			{
				char [ ] temporary = byte_8.toCharArray();
				temporary[1] = '1';
				byte_8 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 66 - Start in Ridley's Lair"))
			{
				char [ ] temporary = byte_8.toCharArray();
				temporary[2] = '1';
				byte_8 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 67 - Reset"))
			{
				char [ ] temporary = byte_8.toCharArray();
				temporary[3] = '1';
				byte_8 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 68 - ???"))
			{
				char [ ] temporary = byte_8.toCharArray();
				temporary[4] = '1';
				byte_8 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 69 - ???"))
			{
				char [ ] temporary = byte_8.toCharArray();
				temporary[5] = '1';
				byte_8 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 70 - ???"))
			{
				char [ ] temporary = byte_8.toCharArray();
				temporary[6] = '1';
				byte_8 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 71 - Equip Leotard"))
			{
				char [ ] temporary = byte_8.toCharArray();
				temporary[7] = '1';
				byte_8 = String.valueOf(temporary);
			}
			
			// Checkboxes corresponding to byte 9
			else if (checkbox.getText().equals("Bit 72 - Samus has Bombs"))
			{
				char [ ] temporary = byte_9.toCharArray();
				temporary[0] = '1';
				byte_9 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 73 - Samus has High Jump Boots"))
			{
				char [ ] temporary = byte_9.toCharArray();
				temporary[1] = '1';
				byte_9 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 74 - Samus has Long Beam"))
			{
				char [ ] temporary = byte_9.toCharArray();
				temporary[2] = '1';
				byte_9 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 75 - Samus has Screw Attack"))
			{
				char [ ] temporary = byte_9.toCharArray();
				temporary[3] = '1';
				byte_9 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 76 - Samus has Maru Mari"))
			{
				char [ ] temporary = byte_9.toCharArray();
				temporary[4] = '1';
				byte_9 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 77 - Samus has Varia Suit"))
			{
				char [ ] temporary = byte_9.toCharArray();
				temporary[5] = '1';
				byte_9 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 78 - Samus has Wave Beam"))
			{
				char [ ] temporary = byte_9.toCharArray();
				temporary[6] = '1';
				byte_9 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 79 - Samus has Ice Beam"))
			{
				char [ ] temporary = byte_9.toCharArray();
				temporary[7] = '1';
				byte_9 = String.valueOf(temporary);
			}
			
			// Checkboxes corresponding to byte 15
			else if (checkbox.getText().equals("Bit 80 - ???"))
			{
				char [ ] temporary = byte_15.toCharArray();
				temporary[0] = '1';
				byte_15 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 81 - ???"))
			{
				char [ ] temporary = byte_15.toCharArray();
				temporary[1] = '1';
				byte_15 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 82 - ???"))
			{
				char [ ] temporary = byte_15.toCharArray();
				temporary[2] = '1';
				byte_15 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 83 - ???"))
			{
				char [ ] temporary = byte_15.toCharArray();
				temporary[3] = '1';
				byte_15 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 84 - Killed Ridley"))
			{
				char [ ] temporary = byte_15.toCharArray();
				temporary[4] = '1';
				byte_15 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 85 - Ridley Statue Raised"))
			{
				char [ ] temporary = byte_15.toCharArray();
				temporary[5] = '1';
				byte_15 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 86 - Killed Kraid"))
			{
				char [ ] temporary = byte_15.toCharArray();
				temporary[6] = '1';
				byte_15 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 87 - Kraid Statue Raised"))
			{
				char [ ] temporary = byte_15.toCharArray();
				temporary[7] = '1';
				byte_15 = String.valueOf(temporary);
			}
			
		}
		
		else if (checkbox.isSelected() == false)
		{
			// Checkboxes corresponding to byte 0
			if (checkbox.getText().equals("Bit 0 - Took Maru Mari"))
			{
				char [ ] temporary = byte_0.toCharArray();
				temporary[0] = '0';
				byte_0 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 1 - Missile Container (Brinstar)"))
			{
				char [ ] temporary = byte_0.toCharArray();
				temporary[1] = '0';
				byte_0 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 2 - Red Door (Long Beam)"))
			{
				char [ ] temporary = byte_0.toCharArray();
				temporary[2] = '0';
				byte_0 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 3 - Red Door (Tourian Bridge)"))
			{
				char [ ] temporary = byte_0.toCharArray();
				temporary[3] = '0';
				byte_0 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 4 - Energy Tank (Brinstar)"))
			{
				char [ ] temporary = byte_0.toCharArray();
				temporary[4] = '0';
				byte_0 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 5 - Red Door (Bombs)"))
			{
				char [ ] temporary = byte_0.toCharArray();
				temporary[5] = '0';
				byte_0 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 6 - Took Bombs"))
			{
				char [ ] temporary = byte_0.toCharArray();
				temporary[6] = '0';
				byte_0 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 7 - Red Door (Ice Beam Brinstar)"))
			{
				char [ ] temporary = byte_0.toCharArray();
				temporary[7] = '0';
				byte_0 = String.valueOf(temporary);
			}
			
			// Checkboxes corresponding to byte 1
			else if (checkbox.getText().equals("Bit 8 - Missile Container (Brinstar)"))
			{
				char [ ] temporary = byte_1.toCharArray();
				temporary[0] = '0';
				byte_1 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 9 - Energy Tank (Brinstar)"))
			{
				char [ ] temporary = byte_1.toCharArray();
				temporary[1] = '0';
				byte_1 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 10 - Red Door (Varia Suit)"))
			{
				char [ ] temporary = byte_1.toCharArray();
				temporary[2] = '0';
				byte_1 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 11 - Took Varia Suit"))
			{
				char [ ] temporary = byte_1.toCharArray();
				temporary[3] = '0';
				byte_1 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 12 - Energy Tank (Brinstar)"))
			{
				char [ ] temporary = byte_1.toCharArray();
				temporary[4] = '0';
				byte_1 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 13 - Missile Container (Norfair)"))
			{
				char [ ] temporary = byte_1.toCharArray();
				temporary[5] = '0';
				byte_1 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 14 - Missile Container (Norfair)"))
			{
				char [ ] temporary = byte_1.toCharArray();
				temporary[6] = '0';
				byte_1 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 15 - Red Door (Ice Beam Norfair)"))
			{
				char [ ] temporary = byte_1.toCharArray();
				temporary[7] = '0';
				byte_1 = String.valueOf(temporary);
			}
			
			// Checkboxes corresponding to byte 2
			else if (checkbox.getText().equals("Bit 16 - Missile Container (Norfair)"))
			{
				char [ ] temporary = byte_2.toCharArray();
				temporary[0] = '0';
				byte_2 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 17 - Missile Container (Norfair)"))
			{
				char [ ] temporary = byte_2.toCharArray();
				temporary[1] = '0';
				byte_2 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 18 - Missile Container (Norfair)"))
			{
				char [ ] temporary = byte_2.toCharArray();
				temporary[2] = '0';
				byte_2 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 19 - Missile Container (Norfair)"))
			{
				char [ ] temporary = byte_2.toCharArray();
				temporary[3] = '0';
				byte_2 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 20 - Missile Container (Norfair)"))
			{
				char [ ] temporary = byte_2.toCharArray();
				temporary[4] = '0';
				byte_2 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 21 - Missile Container (Norfair)"))
			{
				char [ ] temporary = byte_2.toCharArray();
				temporary[5] = '0';
				byte_2 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 22 - Missile Container (Norfair)"))
			{
				char [ ] temporary = byte_2.toCharArray();
				temporary[6] = '0';
				byte_2 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 23 - Red Door (High Jump Boots)"))
			{
				char [ ] temporary = byte_2.toCharArray();
				temporary[7] = '0';
				byte_2 = String.valueOf(temporary);
			}
			
			// Checkboxes corresponding to byte 3
			else if (checkbox.getText().equals("Bit 24 - Took High Jump Boots"))
			{
				char [ ] temporary = byte_3.toCharArray();
				temporary[0] = '0';
				byte_3 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 25 - Red Door (Screw Attack)"))
			{
				char [ ] temporary = byte_3.toCharArray();
				temporary[1] = '0';
				byte_3 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 26 - Took Screw Attack"))
			{
				char [ ] temporary = byte_3.toCharArray();
				temporary[2] = '0';
				byte_3 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 27 - Missile Container (Norfair)"))
			{
				char [ ] temporary = byte_3.toCharArray();
				temporary[3] = '0';
				byte_3 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 28 - Missile Container (Norfair)"))
			{
				char [ ] temporary = byte_3.toCharArray();
				temporary[4] = '0';
				byte_3 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 29 - Red Door (Wave Beam)"))
			{
				char [ ] temporary = byte_3.toCharArray();
				temporary[5] = '0';
				byte_3 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 30 - Energy Tank (Norfair)"))
			{
				char [ ] temporary = byte_3.toCharArray();
				temporary[6] = '0';
				byte_3 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 31 - Missile Container (Norfair)"))
			{
				char [ ] temporary = byte_3.toCharArray();
				temporary[7] = '0';
				byte_3 = String.valueOf(temporary);
			}
			
			// Checkboxes corresponding to byte 4
			else if (checkbox.getText().equals("Bit 32 - Red Door (Kraid's Lair)"))
			{
				char [ ] temporary = byte_4.toCharArray();
				temporary[0] = '0';
				byte_4 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 33 - Missile Container (Kraid's Lair)"))
			{
				char [ ] temporary = byte_4.toCharArray();
				temporary[1] = '0';
				byte_4 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 34 - Missile Container (Kraid's Lair)"))
			{
				char [ ] temporary = byte_4.toCharArray();
				temporary[2] = '0';
				byte_4 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 35 - Red Door (Kraid's Lair)"))
			{
				char [ ] temporary = byte_4.toCharArray();
				temporary[3] = '0';
				byte_4 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 36 - Energy Tank (Kraid's Lair)"))
			{
				char [ ] temporary = byte_4.toCharArray();
				temporary[4] = '0';
				byte_4 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 37 - Red Door (Kraid's Lair)"))
			{
				char [ ] temporary = byte_4.toCharArray();
				temporary[5] = '0';
				byte_4 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 38 - Red Door (Kraid's Lair)"))
			{
				char [ ] temporary = byte_4.toCharArray();
				temporary[6] = '0';
				byte_4 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 39 - Missile Container (Kraid's Lair)"))
			{
				char [ ] temporary = byte_4.toCharArray();
				temporary[7] = '0';
				byte_4 = String.valueOf(temporary);
			}
			
			// Checkboxes corresponding to byte 5
			else if (checkbox.getText().equals("Bit 40 - Missile Container (Kraid's Lair)"))
			{
				char [ ] temporary = byte_5.toCharArray();
				temporary[0] = '0';
				byte_5 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 41 - Red Door (Kraid's Room)"))
			{
				char [ ] temporary = byte_5.toCharArray();
				temporary[1] = '0';
				byte_5 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 42 - Energy Tank (Kraid's Room)"))
			{
				char [ ] temporary = byte_5.toCharArray();
				temporary[2] = '0';
				byte_5 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 43 - Missile Container (Ridley's Lair)"))
			{
				char [ ] temporary = byte_5.toCharArray();
				temporary[3] = '0';
				byte_5 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 44 - Red Door (Ridley's Lair)"))
			{
				char [ ] temporary = byte_5.toCharArray();
				temporary[4] = '0';
				byte_5 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 45 - Energy Tank (Ridley's Lair)"))
			{
				char [ ] temporary = byte_5.toCharArray();
				temporary[5] = '0';
				byte_5 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 46 - Missile Container (Ridley's Lair)"))
			{
				char [ ] temporary = byte_5.toCharArray();
				temporary[6] = '0';
				byte_5 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 47 - Yellow Door (Ridley's Room)"))
			{
				char [ ] temporary = byte_5.toCharArray();
				temporary[7] = '0';
				byte_5 = String.valueOf(temporary);
			}
			
			// Checkboxes corresponding to byte 6
			else if (checkbox.getText().equals("Bit 48 - Energy Tank (Room behind Ridley)"))
			{
				char [ ] temporary = byte_6.toCharArray();
				temporary[0] = '0';
				byte_6 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 49 - Missile Container (Ridley's Lair)"))
			{
				char [ ] temporary = byte_6.toCharArray();
				temporary[1] = '0';
				byte_6 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 50 - Yellow Door (Tourian)"))
			{
				char [ ] temporary = byte_6.toCharArray();
				temporary[2] = '0';
				byte_6 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 51 - Red Door (Tourian)"))
			{
				char [ ] temporary = byte_6.toCharArray();
				temporary[3] = '0';
				byte_6 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 52 - Red Door (Tourian)"))
			{
				char [ ] temporary = byte_6.toCharArray();
				temporary[4] = '0';
				byte_6 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 53 - Killed Zebetite 1"))
			{
				char [ ] temporary = byte_6.toCharArray();
				temporary[5] = '0';
				byte_6 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 54 - Killed Zebetite 2"))
			{
				char [ ] temporary = byte_6.toCharArray();
				temporary[6] = '0';
				byte_6 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 55 - Killed Zebetite 3"))
			{
				char [ ] temporary = byte_6.toCharArray();
				temporary[7] = '0';
				byte_6 = String.valueOf(temporary);
			}
			
			// Checkboxes corresponding to byte 7
			else if (checkbox.getText().equals("Bit 56 - Killed Zebetite 4"))
			{
				char [ ] temporary = byte_7.toCharArray();
				temporary[0] = '0';
				byte_7 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 57 - Killed Zebetite 5"))
			{
				char [ ] temporary = byte_7.toCharArray();
				temporary[1] = '0';
				byte_7 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 58 - Killed Mother Brain"))
			{
				char [ ] temporary = byte_7.toCharArray();
				temporary[2] = '0';
				byte_7 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 59 - ???"))
			{
				char [ ] temporary = byte_7.toCharArray();
				temporary[3] = '0';
				byte_7 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 60 - ???"))
			{
				char [ ] temporary = byte_7.toCharArray();
				temporary[4] = '0';
				byte_7 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 61 - ???"))
			{
				char [ ] temporary = byte_7.toCharArray();
				temporary[5] = '0';
				byte_7 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 62 - ???"))
			{
				char [ ] temporary = byte_7.toCharArray();
				temporary[6] = '0';
				byte_7 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 63 - ???"))
			{
				char [ ] temporary = byte_7.toCharArray();
				temporary[7] = '0';
				byte_7 = String.valueOf(temporary);
			}
			
			// Checkboxes corresponding to byte 8
			else if (checkbox.getText().equals("Bit 64 - Start in Norfair"))
			{
				char [ ] temporary = byte_8.toCharArray();
				temporary[0] = '0';
				byte_8 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 65 - Start in Kraid's Lair"))
			{
				char [ ] temporary = byte_8.toCharArray();
				temporary[1] = '0';
				byte_8 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 66 - Start in Ridley's Lair"))
			{
				char [ ] temporary = byte_8.toCharArray();
				temporary[2] = '0';
				byte_8 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 67 - Reset"))
			{
				char [ ] temporary = byte_8.toCharArray();
				temporary[3] = '0';
				byte_8 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 68 - ???"))
			{
				char [ ] temporary = byte_8.toCharArray();
				temporary[4] = '0';
				byte_8 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 69 - ???"))
			{
				char [ ] temporary = byte_8.toCharArray();
				temporary[5] = '0';
				byte_8 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 70 - ???"))
			{
				char [ ] temporary = byte_8.toCharArray();
				temporary[6] = '0';
				byte_8 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 71 - Equip Leotard"))
			{
				char [ ] temporary = byte_8.toCharArray();
				temporary[7] = '0';
				byte_8 = String.valueOf(temporary);
			}
			
			// Checkboxes corresponding to byte 9
			else if (checkbox.getText().equals("Bit 72 - Samus has Bombs"))
			{
				char [ ] temporary = byte_9.toCharArray();
				temporary[0] = '0';
				byte_9 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 73 - Samus has High Jump Boots"))
			{
				char [ ] temporary = byte_9.toCharArray();
				temporary[1] = '0';
				byte_9 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 74 - Samus has Long Beam"))
			{
				char [ ] temporary = byte_9.toCharArray();
				temporary[2] = '0';
				byte_9 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 75 - Samus has Screw Attack"))
			{
				char [ ] temporary = byte_9.toCharArray();
				temporary[3] = '0';
				byte_9 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 76 - Samus has Maru Mari"))
			{
				char [ ] temporary = byte_9.toCharArray();
				temporary[4] = '0';
				byte_9 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 77 - Samus has Varia Suit"))
			{
				char [ ] temporary = byte_9.toCharArray();
				temporary[5] = '0';
				byte_9 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 78 - Samus has Wave Beam"))
			{
				char [ ] temporary = byte_9.toCharArray();
				temporary[6] = '0';
				byte_9 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 79 - Samus has Ice Beam"))
			{
				char [ ] temporary = byte_9.toCharArray();
				temporary[7] = '0';
				byte_9 = String.valueOf(temporary);
			}
			
			// Checkboxes corresponding to byte 15
			else if (checkbox.getText().equals("Bit 80 - ???"))
			{
				char [ ] temporary = byte_15.toCharArray();
				temporary[0] = '0';
				byte_15 = String.valueOf(temporary); 
			}
			
			else if (checkbox.getText().equals("Bit 81 - ???"))
			{
				char [ ] temporary = byte_15.toCharArray();
				temporary[1] = '0';
				byte_15 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 82 - ???"))
			{
				char [ ] temporary = byte_15.toCharArray();
				temporary[2] = '0';
				byte_15 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 83 - ???"))
			{
				char [ ] temporary = byte_15.toCharArray();
				temporary[3] = '0';
				byte_15 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 84 - Killed Ridley"))
			{
				char [ ] temporary = byte_15.toCharArray();
				temporary[4] = '0';
				byte_15 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 85 - Ridley Statue Raised"))
			{
				char [ ] temporary = byte_15.toCharArray();
				temporary[5] = '0';
				byte_15 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 86 - Killed Kraid"))
			{
				char [ ] temporary = byte_15.toCharArray();
				temporary[6] = '0';
				byte_15 = String.valueOf(temporary);
			}
			
			else if (checkbox.getText().equals("Bit 87 - Kraid Statue Raised"))
			{
				char [ ] temporary = byte_15.toCharArray();
				temporary[7] = '0';
				byte_15 = String.valueOf(temporary);
			}
			
			
		}
		
	}
	}
	}
	
/********************************************************************************************/	
	public static void main (String args[])
	{

		UIManager.put("OptionPane.background", Color.WHITE);
		UIManager.put("Panel.background", Color.WHITE); 
		UIManager.put("OptionPane.messageFont", new Font("Courier New", Font.BOLD, 14)); 
		UIManager.put("OptionPane.buttonFont", new Font("Courier New", Font.PLAIN, 12)); 
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new Driver(); 
			} 
		}); 
	}

@Override
public void actionPerformed(ActionEvent event) { 
	// TODO Auto-generated method stub
	// Missile Count
			if (event.getSource() == set_missile_count)
			{
				
				String input = missile_count_TF.getText();
				
				if (input.length()==0)
				{
					missile_count_string = "00000000";
					return;
				}
				
				char [] array = input.toCharArray();
				
				for (int counter = 0; counter < input.length(); counter++)
				{
					if (Character.isDigit(array[counter]) == false)
					{
						JOptionPane.showMessageDialog(Driver.this, "Input can only have numbers");  
						return;
					}
				}
				
				int temp = Integer.parseInt(input);
				
				if (temp < 0 || temp > 255)
				{
					JOptionPane.showMessageDialog(Driver.this, "Range is 0 <= input <= 255"); 
					return;
				}
				
				String temp2 = Integer.toBinaryString(temp);
				temp2 = StringUtils.leftPad(temp2, 8, "0");
				
				missile_count_string = temp2;
			} 
			
			/* Game Time (measured in "ticks", 1 tick is approximately 4.267 seconds on a North American NES or
			 * 5.12 seconds on a PAL NES. Time is stored in little-endian format.
			 */
			else if (event.getSource() == set_game_time)
			{
				
				String input = game_time_TF.getText();
				
				if (input.length()==0)
				{
					game_time_string = "00000000000000000000000000000000"; 
					return;
				}
				
				char [] array = input.toCharArray();
				
				for (int counter = 0; counter < input.length(); counter++)
				{
					if (Character.isDigit(array[counter]) == false)
					{
						JOptionPane.showMessageDialog(Driver.this, "Input can only have numbers");  
						return;
					} 
				}
				
				Long temp = Long.parseLong(input);
				
				if (temp < 0 || temp > 4294967925L)
				{
					JOptionPane.showMessageDialog(Driver.this, "Range is 0 <= input <= 4294967925"); 
					return;
				}
				
				String temp2 = Long.toBinaryString(temp);
				temp2 = StringUtils.leftPad(temp2, 32, "0");

				String first, second, third, fourth;
				first = new StringBuilder(temp2.substring(0,8)).reverse().toString();
				second = new StringBuilder(temp2.substring(8,16)).reverse().toString();
				third = new StringBuilder(temp2.substring(16,24)).reverse().toString();
				fourth = new StringBuilder(temp2.substring(24,32)).reverse().toString();
				
				// Concatenate all of the reverse bytes together and reverse the result once more
				game_time_string = new StringBuilder(first + second + third + fourth).reverse().toString();
				
				
	
}
}
}
